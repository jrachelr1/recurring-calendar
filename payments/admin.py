from django.contrib import admin

from payments.models import Payment

# Register your models here.
class PaymentAdmin(admin.ModelAdmin):
    pass


admin.site.register(Payment, PaymentAdmin)
