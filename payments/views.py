from django.shortcuts import render
from django.views.generic.list import ListView
from payments.models import Payment
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView, DeleteView
from django.urls import reverse_lazy


# Create your views here.
class PaymentsListView(LoginRequiredMixin, ListView):
    model = Payment
    template_name = "payments/payments_list.html"

    def get_queryset(self):
        return Payment.objects.filter(owner=self.request.user)


class PaymentCreateView(LoginRequiredMixin, CreateView):
    model = Payment
    template_name = "payments/create_payment.html"
    fields = [
        "name",
        "amount",
        "date_due",
        "owner",
        "description",
        "category",
        "recurring",
    ]

    def get_success_url(self):
        return reverse_lazy("payments_list")
