from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Payment(models.Model):
    name = models.CharField(max_length=100)
    amount = models.IntegerField(null=True)
    date_due = models.DateField()
    # how to set due date to a recurring day/month or custom
    owner = models.ForeignKey(User, related_name="payments", on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    category = models.CharField(max_length=100, null=True)
    recurring = models.BooleanField(default=False)

    def __str__(self):
        return self.name
