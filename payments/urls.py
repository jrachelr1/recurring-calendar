from django.urls import path
from payments.views import PaymentsListView, PaymentCreateView

urlpatterns = [
    path("mypayments/", PaymentsListView.as_view(), name="payments_list"),
    path("createpayment/", PaymentCreateView.as_view(), name="create_payment"),
]
