from django.urls import path
from . import views
from cal.views import CalendarView

# app_name = "cal"
urlpatterns = [
    path("index", views.index, name="index"),
    path("calendar/", CalendarView.as_view(), name="calendar"),
]
